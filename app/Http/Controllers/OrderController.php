<?php


namespace App\Http\Controllers;

use App\Http\Services\ApiResponseService;
use App\Http\Services\OrderService;
use Illuminate\Http\JsonResponse;
use \Illuminate\Http\Request;

class OrderController extends Controller
{
    protected OrderService $orderService;
    protected ApiResponseService $apiResponseService;

    public function __construct(
        OrderService $orderService,
        ApiResponseService $apiResponseService
    ) {
        $this->orderService = $orderService;
        $this->apiResponseService = $apiResponseService;
    }

    public function index(): JsonResponse
    {
        $orders = $this->orderService->getOrders();

        return $this->apiResponseService->successResponse($orders, '200');
    }

    public function show($order): JsonResponse
    {
        $order = $this->orderService->getOrder($order);

        return $this->apiResponseService->successResponse($order, '200');
    }

    public function store(Request $request): JsonResponse
    {
        $response = $this->orderService->setOrder($request);

        return $this->apiResponseService->successResponse($response, '200');
    }

    public function update(Request $request, $order): JsonResponse
    {
        $response = $this->orderService->updateOrder($request, $order);

        return $this->apiResponseService->successResponse($response, '200');
    }

    public function destroy($order): JsonResponse
    {
        $response = $this->orderService->deleteOrder($order);

        return $this->apiResponseService->successResponse($response, '200');
    }
}