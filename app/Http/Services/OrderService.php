<?php

namespace App\Http\Services;

use App\Http\Repositories\OrderRepository;

class OrderService
{
    protected OrderRepository $orderRepository;

    public function __construct(
        OrderRepository $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    public function getOrders(): ?array
    {
        return $this->orderRepository->getOrders();
    }

    public function getOrder($order): ?array
    {
        return $this->orderRepository->getOrder($order);
    }

    public function setOrder($request): string
    {
        return $this->orderRepository->setOrder($request);
    }

    public function updateOrder($request, $id): string
    {
        return $this->orderRepository->updateOrder($request, $id);
    }

    public function deleteOrder($id): string
    {
        return $this->orderRepository->deleteOrder($id);
    }
}