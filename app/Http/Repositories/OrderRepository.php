<?php

namespace App\Http\Repositories;

use App\Models\Order;

class OrderRepository
{
    public function getOrders(): ?array
    {
        return Order::query()->get()->toArray();
    }

    public function getOrder($order): ?array
    {
        return Order::query()->find($order)->toArray();
    }

    public function setOrder($request): string
    {
        $order = new Order();
        $order->fill($request->toArray());
        $saved = $order->save();

        if ($saved === 0) {
            return 'false';
        }

        return 'true';
    }

    public function updateOrder($request, $id): string
    {
        $updated = Order::query()->find($id)->update(
            $request->toArray()
        );

        if ($updated === 0) {
            return 'false';
        }

        return 'true';
    }

    public function deleteOrder($id): string
    {
        $deleted = Order::query()->find($id)->delete();

        if ($deleted === 0) {
            return 'false';
        }

        return 'true';
    }
}