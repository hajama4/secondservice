<?php


namespace Database\Factories;


use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    public function definition()
    {
        return [
            'quantity' => $this->faker->numberBetween(1,10),
            'discount' => $this->faker->numberBetween(0,100),
            'total_price' => $this->faker->numberBetween(1, 200),
        ];
    }
}